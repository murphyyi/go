package account

import (
	orm "xxx/+core/orm"
)

// User model
type User struct {
	orm.Base
	Name   string `json:"name"`
	Email  string `json:"email"`
	Age    int    `json:"age"`
	Active bool   `json:"active"`
}

// UserFilter model
type UserFilter struct {
	orm.Filter
}
