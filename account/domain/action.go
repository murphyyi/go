package account

// Actions
const (
	// ActionResetUserCounter -> reset counter
	ActionResetUserCounter = "ActionResetUserCounter"

	// ActionIncreaseUserCounter -> increase counter
	ActionIncreaseUserCounter = "ActionIncreaseUserCounter"

	// ActionDecreaseUserCounter -> decrease counter
	ActionDecreaseUserCounter = "ActionDecreaseUserCounter"

	// ActionVerifyUser -> verify user
	ActionVerifyUser = "ActionVerifyUser"

	// ActionRollbackUserCreation -> rollback
	ActionRollbackUserCreation = "ActionRollbackUserCreation"
)
