package query

import (
	"context"

	"github.com/go-kit/kit/log"

	domain "xxx/account/domain"
)

// UserQueryHandler is to handle GetUserQuery
type UserQueryHandler struct {
	logger     log.Logger
	repository domain.Repository
}

/**
 * NewUserQueryHandler initialization
 *
 * @param  {Repository} repo 								The repository
 * @return {NewUserQueryHandler} 						The query handler
 */
func NewUserQueryHandler(repo domain.Repository) UserQueryHandler {
	return UserQueryHandler{
		repository: repo,
	}
}

/**
 * Get user by id
 *
 * @param  {Context} ctx           	The context
 * @param  {string} id          		The user id
 * @return {(User, error)}       		Success: The created user; Failed: The error message
 */
func (handler UserQueryHandler) Handle(ctx context.Context, id string) (domain.User, error) {
	user, err := handler.repository.GetUser(ctx, id)

	return user, err
}
