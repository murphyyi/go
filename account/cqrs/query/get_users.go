package query

import (
	"context"

	orm "xxx/+core/orm"
	automapper "xxx/+core/utils/automapper"
	domain "xxx/account/domain"
)

// UsersQuery is to create new user
type UsersQuery struct {
	Limit  int
	Page   int
	SortBy string
	Search string
}

// UsersQueryHandler is to handle AllUsersQuery
type UsersQueryHandler struct {
	repository domain.Repository
}

/**
 * NewUsersQueryHandler initialization
 *
 * @param  {Repository} repo 								The repository
 * @return {NewUsersQueryHandler} 					The query handler
 */
func NewUsersQueryHandler(repo domain.Repository) UsersQueryHandler {
	return UsersQueryHandler{
		repository: repo,
	}
}

/**
 * Get all users
 *
 * @param  {Context} ctx           	The context
 * @param  {AllUsersQuery} query    The query
 * @return {(Paginator, error)} 		Success -> paginator info; Failed -> error
 */
func (handler UsersQueryHandler) Handle(ctx context.Context, query UsersQuery) (orm.Paginator, error) {
	filters := domain.UserFilter{}
	automapper.Map(&query, &filters)
	paginator, err := handler.repository.GetUsers(ctx, filters)

	return paginator, err
}
