package command

import (
	"context"

	"github.com/google/uuid"

	orm "xxx/+core/orm"
	domain "xxx/account/domain"
)

// CreateUserCommand is to create new user
type CreateUserCommand struct {
	TransactionID uuid.UUID
	ID            uuid.UUID
	Name          string
	Email         string
	Age           int
}

// CreateUserCommandHandler is to handle CreateUserCommand
type CreateUserCommandHandler struct {
	repository domain.Repository
}

/**
 * NewCreateUserCommandHandler initialization
 *
 * @param  {Repository} repo 								The repository
 * @return {CreateUserCommandHandler} 			The command handler
 */
func NewCreateUserCommandHandler(repo domain.Repository) CreateUserCommandHandler {
	return CreateUserCommandHandler{
		repository: repo,
	}
}

/**
 * Create a user
 *
 * @param  {Context} ctx           					The context
 * @param  {CreateUserCommand} command     	The create user command
 * @return {(User, error)}       						Success: The created user; Failed: The error message
 */
func (handler CreateUserCommandHandler) Handle(ctx context.Context, command CreateUserCommand) (domain.User, error) {
	user := domain.User{
		Base:   orm.Base{ID: command.ID},
		Name:   command.Name,
		Email:  command.Email,
		Age:    command.Age,
		Active: false,
	}
	createdUser, err := handler.repository.CreateUser(ctx, user)

	return createdUser, err
}
