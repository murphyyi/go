package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/nats-io/stan.go"

	// Import GORM-related packages.
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	envutil "xxx/+core/utils/env"
	natsutil "xxx/+core/utils/nats"
	account "xxx/account"
	accountCQRS "xxx/account/cqrs"
	accountDomain "xxx/account/domain"
	accountSaga "xxx/account/saga"
	transport "xxx/account/transport"
	httpTransport "xxx/account/transport/http"
)

func main() {
	flag.Parse()

	// Init logger
	logger := initLogger()

	// Init db
	db := initDatabase(logger)

	// Init NATS Streaming component
	streamingComp := initNATSStreaming(logger)

	// Init error channel
	errorChannel := make(chan error)

	// Init repository
	repository := accountDomain.NewRepository(db, logger)

	// Init cqrs facade: commands & queries
	cqrsFacade := accountCQRS.NewCQRSFacade(repository)

	// Init server
	service := account.NewService(logger, cqrsFacade, streamingComp)

	// Init sagas
	go initSagas(logger, streamingComp, service)

	// Init signal notifications
	go initSignalNotifications(errorChannel)

	// Init HTTP Server
	go initHTTPServer(service, errorChannel)

	// Cleanup
	<-errorChannel

	streamingComp.Shutdown()
	level.Error(logger).Log("exit")
}

/**
 * Init orchestrator sagas
 *
 * @param  {StreamingComponent} streamingComp 		The NATS Streaming component
 * @param  {Service} service											The service
 * @return {void}
 */
func initSagas(logger log.Logger, streamingComp *natsutil.StreamingComponent, service account.Service) {
	accountSaga.InitSagas(logger, streamingComp, service)
}

/**
 * Init NATS Streaming component
 *
 * @param  {logger log.Logger} logger 	The logger
 * @return {*StreamingComponent} 				The NATS Streaming component
 */
func initNATSStreaming(logger log.Logger) *natsutil.StreamingComponent {
	// Register new component within the NATS system.
	comp := natsutil.NewStreamingComponent("account-api")

	// Connect to NATS
	err := comp.ConnectToNATSStreaming(
		envutil.GetEnv("NATS_CLUSTER_ID", "test-cluster"),
		stan.NatsURL(stan.DefaultNatsURL),
	)

	if err != nil {
		level.Error(logger).Log(
			"Connect-to-NATS-Streaming-Server-failed", comp.Name(),
			"Error:", err,
		)
	}

	logger.Log("NATS-Streaming-Server-connected", comp.Name())

	return comp
}

/**
 * Init signal notifications
 *
 * @param  {chan error} errorChannel 		The error channel
 * @return {void}
 */
func initSignalNotifications(errorChannel chan error) {
	signalChannel := make(chan os.Signal, 1)

	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)
	errorChannel <- fmt.Errorf("%s", <-signalChannel)
}

/**
 * Init http server
 *
 * @param  {Service} service 						The service
 * @param  {chan error} errorChannel 		The error channel
 * @return {void}
 */
func initHTTPServer(service account.Service, errorChannel chan error) {
	ctx := context.Background()
	port := envutil.GetEnv("PORT", "17601")
	httpAddr := flag.String("http", ":"+port, "http listen address")
	endpoints := transport.MakeEndpoints(service)

	fmt.Println("listening on port", *httpAddr)
	routesHandler := httpTransport.InitRoutes(ctx, endpoints)
	errorChannel <- http.ListenAndServe(*httpAddr, routesHandler)
}

/**
 * Init logger
 *
 * @return {log.Logger} 	The structed logger
 */
func initLogger() log.Logger {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"service", "account",
			"timestamp:", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	level.Info(logger).Log("msg", "service started")
	defer level.Info(logger).Log("msg", "service ended")

	return logger
}

/**
 * Initialize database
 *
 * @param  {log.Logger} logger 			The logger
 * @return {void}
 */
func initDatabase(logger log.Logger) *gorm.DB {
	sqlDB, err := sql.Open(envutil.GetEnv("DB_DRIVER", "postgres"), envutil.GetEnv("DB_SOURCE", "postgresql://xxx@localhost:26257/xxx?sslmode=disable"))
	db, err := gorm.Open(postgres.New(postgres.Config{
		Conn: sqlDB,
	}), &gorm.Config{})

	if err != nil {
		level.Error(logger).Log("Error connecting to the database: ", err)
		os.Exit(-1)
	}

	// Auto migrate the schema
	db.AutoMigrate(&accountDomain.User{})
	db.AutoMigrate(&accountDomain.UserCounter{})

	return db
}
