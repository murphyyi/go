package saga

import (
	"encoding/json"

	"github.com/go-kit/kit/log"
	"github.com/google/uuid"
	"github.com/nats-io/stan.go"

	natsutil "xxx/+core/utils/nats"
	account "xxx/account"
	domain "xxx/account/domain"
)

const (
	DeleteUserOrchestratorDurableID string = "delete-user-orchestrator"
)

// DeleteUserOrchestrator orchestrates the order processing task
type DeleteUserOrchestrator struct {
	logger             log.Logger
	streamingComponent *natsutil.StreamingComponent
	service            account.Service
}

/**
 * NewDeleteUserSaga
 *
 * @param  {Logger} logger 									The logger
 * @param  {Service} service 								The service
 * @param  {StreamingComponent} natsComp 		The NATS Streaming component
 * @return {Repository} 										The repository instance
 */
func NewDeleteUserSaga(logger log.Logger, natsComp *natsutil.StreamingComponent, service account.Service) DeleteUserOrchestrator {
	orchestrator := DeleteUserOrchestrator{
		logger:             log.With(logger, "Context", "DeleteUserOrchestrator", "streaming-component", natsComp.Name()),
		streamingComponent: natsComp,
		service:            service,
	}

	orchestrator.start()

	return orchestrator
}

// Start to orchestrate the process
func (orchestrator DeleteUserOrchestrator) start() {
	go orchestrator.handleEvents()
}

// The orchestrator handles the user deletion flow
func (orchestrator DeleteUserOrchestrator) handleEvents() {
	sc := orchestrator.streamingComponent.NATS()

	for _, event := range []string{domain.UserDeletedEvent} {
		sc.QueueSubscribe(event, QueueGroup, func(msg *stan.Msg) {
			message := natsutil.Message{}
			err := json.Unmarshal(msg.Data, &message)

			orchestrator.logger.Log("===================================")

			if err != nil || message.Payload == nil || message.ID == uuid.Nil {
				orchestrator.logger.Log("Bad-message", msg)
				return
			}

			// Handle the message
			orchestrator.logger.Log("Raw-message", msg)

			switch msg.Subject {
			case domain.UserDeletedEvent:
				{
					orchestrator.handleUserDeleted(message)
					break
				}
			}
		}, stan.DurableName(DeleteUserOrchestratorDurableID))
	}
}

// Handle when user deleted
// - Decrease counter
func (orchestrator DeleteUserOrchestrator) handleUserDeleted(message natsutil.Message) error {
	orchestrator.streamingComponent.PublishMessage(domain.ActionDecreaseUserCounter, message)

	return nil
}
