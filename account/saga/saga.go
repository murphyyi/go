package saga

import (
	natsutil "xxx/+core/utils/nats"

	"github.com/go-kit/kit/log"

	account "xxx/account"
)

/**
 * Init orchestrator sagas
 *
 * @param  {StreamingComponent} streamingComp 		The NATS Streaming component
 * @param  {Service} service											The service
 * @return {void}
 */
func InitSagas(logger log.Logger, streamingComp *natsutil.StreamingComponent, service account.Service) {
	NewCreateUserSaga(logger, streamingComp, service)
	NewDeleteUserSaga(logger, streamingComp, service)
}
