package automapper

import (
	"encoding/json"

	"github.com/mitchellh/mapstructure"
)

// Map to auto map fields from source to dests
func Map(source interface{}, dest interface{}) error {
	type tmp interface{}
	g := tmp(source)

	first, err := json.Marshal(g)
	if err != nil {
		return err
	}

	second, err := json.Marshal(source)
	if err != nil {
		return err
	}

	data := make(map[string]interface{})
	json.Unmarshal(first, &data)
	json.Unmarshal(second, &data)

	converted, err := json.Marshal(data)

	json.Unmarshal(converted, dest)

	return err
}

// MapStructure handles map -> struct
func MapStructure(source interface{}, dest interface{}) {
	mapstructure.Decode(source, &dest)
}
