package orm

import (
	"math"

	"gorm.io/gorm"
)

// Filter model
type Filter struct {
	Limit  int
	Page   int
	SortBy string
	Search string
}

// Paginator model
type Paginator struct {
	TotalRecord int64       `json:"totalRecord"`
	TotalPage   int         `json:"totalPage"`
	Offset      int         `json:"offset"`
	Limit       int         `json:"limit"`
	Page        int         `json:"page"`
	PrevPage    int         `json:"prevPage"`
	NextPage    int         `json:"nextPage"`
	Records     interface{} `json:"records"`
}

// PagingParam model
type PagingParam struct {
	DB      *gorm.DB
	Page    int
	Limit   int
	ShowSQL bool
}

// Paging is to calculate result
func Paging(param *PagingParam, result interface{}) (Paginator, error) {
	db := param.DB

	if param.ShowSQL {
		db = db.Debug()
	}

	if param.Page < 1 {
		param.Page = 1
	}

	if param.Limit <= 0 {
		param.Limit = 10
	}

	done := make(chan bool, 1)
	var paginator Paginator
	var count int64
	var offset int

	go countRecords(db, result, done, &count)

	if param.Page == 1 {
		offset = 0
	} else {
		offset = (param.Page - 1) * param.Limit
	}

	tx := db.Limit(param.Limit).Offset(offset).Find(result)
	<-done

	if tx.Error != nil {
		return paginator, tx.Error
	}

	paginator.TotalRecord = count
	paginator.Records = result
	paginator.Page = param.Page
	paginator.Offset = offset
	paginator.Limit = param.Limit
	paginator.TotalPage = int(math.Ceil(float64(count) / float64(param.Limit)))

	if param.Page > 1 {
		paginator.PrevPage = param.Page - 1
	} else {
		paginator.PrevPage = param.Page
	}

	if param.Page == paginator.TotalPage {
		paginator.NextPage = param.Page
	} else {
		paginator.NextPage = param.Page + 1
	}

	return paginator, nil
}

func countRecords(db *gorm.DB, anyType interface{}, done chan bool, count *int64) {
	db.Model(anyType).Count(count)
	done <- true
}
