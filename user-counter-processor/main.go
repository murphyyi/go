package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/uuid"
	stan "github.com/nats-io/stan.go"

	// Import GORM-related packages.
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	envutil "xxx/+core/utils/env"
	natsutil "xxx/+core/utils/nats"
	account "xxx/account/domain"
)

const (
	UserCounterIncreasedEvent = "UserCounterIncreasedEvent"
	UserCounterDecreasedEvent = "UserCounterDecreasedEvent"
	ClientID                  = "user-counter-processor"
	DurableID                 = "user-counter-processor-durable"
	QueueGroup                = "user-queue-group"
)

type userCounterProcessor struct {
	db            *gorm.DB
	streamingComp *natsutil.StreamingComponent
	errorChannel  chan error
}

func main() {
	// Register new NATS component within the system.
	streamingComp := natsutil.NewStreamingComponent(ClientID)

	// Connect to NATS Streaming server
	err := streamingComp.ConnectToNATSStreaming(
		envutil.GetEnv("NATS_CLUSTER_ID", "test-cluster"),
		stan.NatsURL(stan.DefaultNatsURL),
	)

	if err != nil {
		log.Fatal(err)
	}

	// Init processor
	processor := userCounterProcessor{
		db:            initDatabase(),
		streamingComp: streamingComp,
		errorChannel:  make(chan error),
	}

	log.Printf("%s started", ClientID)

	// Init error channel

	// Init signal notifications
	go processor.initSignalNotifications()

	// Subscribe on action reset counter
	go processor.subscribeActionResetUserCounter()

	// Subscribe on action increase counter
	go processor.subscribeActionIncreaseUserCounter()

	// Subscribe on action decrease counter
	go processor.subscribeActionDecreaseUserCounter()

	// Subscribe on action rollback
	go processor.subscribeActionRollbackUserCreation()

	// Cleanup
	<-processor.errorChannel

	processor.streamingComp.Shutdown()

	log.Printf("%s stopped", ClientID)
}

// Initialize database
func initDatabase() *gorm.DB {
	sqlDB, err := sql.Open(envutil.GetEnv("DB_DRIVER", "postgres"), envutil.GetEnv("DB_SOURCE", "postgresql://xxx@localhost:26257/xxx?sslmode=disable"))
	db, err := gorm.Open(postgres.New(postgres.Config{
		Conn: sqlDB,
	}), &gorm.Config{})

	if err != nil {
		log.Fatal("Error connecting to the database: ", err)
		os.Exit(-1)
	}

	// Auto migrate the schema
	db.AutoMigrate(&account.UserCounter{})

	return db
}

// Init signal notifications
func (processor userCounterProcessor) initSignalNotifications() {
	signalChannel := make(chan os.Signal, 1)

	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)
	processor.errorChannel <- fmt.Errorf("%s", <-signalChannel)
}

func (processor userCounterProcessor) getUserCounter() (account.UserCounter, error) {
	var counter account.UserCounter

	counterResult := processor.db.FirstOrCreate(&counter)
	if counterResult.Error != nil {
		log.Panic("Counter not found: ", counterResult.Error)
	}

	return counter, counterResult.Error
}

func (processor userCounterProcessor) updateUserCounter(counter account.UserCounter) (account.UserCounter, error) {
	updateResult := processor.db.Save(&counter)

	if updateResult.Error != nil {
		log.Panic("Update counter failed: ", updateResult.Error)
	}

	return counter, updateResult.Error
}

// Handle ActionResetUserCounter
func (processor userCounterProcessor) subscribeActionResetUserCounter() {
	// Get the NATS Streaming Connection
	sc := processor.streamingComp.NATS()
	sc.QueueSubscribe(account.ActionResetUserCounter, QueueGroup, func(msg *stan.Msg) {
		message := natsutil.Message{}
		err := json.Unmarshal(msg.Data, &message)

		if err != nil || message.Payload == nil || message.ID == uuid.Nil {
			log.Printf("%s received bad message: %+v\n", processor.streamingComp.Name(), msg)
			return
		}

		// Handle the message
		log.Printf("%s subscribed message: %+v\n", processor.streamingComp.Name(), msg)

		// Store counter to DB
		counter, counterErr := processor.getUserCounter()

		if counterErr != nil {
			return
		}

		value := int64(message.Payload.(float64))
		counter.All = value
		_, updateCounterErr := processor.updateUserCounter(counter)

		if updateCounterErr != nil {
			return
		}

		// Publish new event
		processor.streamingComp.PublishMessage(UserCounterIncreasedEvent, natsutil.Message{
			ID:      message.ID,
			Payload: message.Payload,
		})
	}, stan.DurableName(DurableID))
}

// Handle ActionIncreaseUserCounter
func (processor userCounterProcessor) subscribeActionIncreaseUserCounter() {
	// Get the NATS Streaming Connection
	sc := processor.streamingComp.NATS()
	sc.QueueSubscribe(account.ActionIncreaseUserCounter, QueueGroup, func(msg *stan.Msg) {
		message := natsutil.Message{}
		err := json.Unmarshal(msg.Data, &message)

		if err != nil || message.Payload == nil || message.ID == uuid.Nil {
			log.Printf("%s received bad message: %+v\n", processor.streamingComp.Name(), msg)
			return
		}

		// Handle the message
		log.Printf("%s subscribed message: %+v\n", processor.streamingComp.Name(), msg)

		// Store counter to DB
		counter, counterErr := processor.getUserCounter()

		if counterErr != nil {
			return
		}

		counter.All++
		_, updateCounterErr := processor.updateUserCounter(counter)

		if updateCounterErr != nil {
			return
		}

		// Publish new event
		processor.streamingComp.PublishMessage(UserCounterIncreasedEvent, natsutil.Message{
			ID:      message.ID,
			Payload: message.Payload,
		})
	}, stan.DurableName(DurableID))
}

// Handle ActionDecreaseUserCounter
func (processor userCounterProcessor) subscribeActionDecreaseUserCounter() {
	// Get the NATS Streaming Connection
	sc := processor.streamingComp.NATS()
	sc.QueueSubscribe(account.ActionDecreaseUserCounter, QueueGroup, func(msg *stan.Msg) {
		message := natsutil.Message{}
		err := json.Unmarshal(msg.Data, &message)

		if err != nil || message.Payload == nil || message.ID == uuid.Nil {
			log.Printf("%s received bad message: %+v\n", processor.streamingComp.Name(), msg)
			return
		}

		// Handle the message
		log.Printf("%s subscribed message: %+v\n", processor.streamingComp.Name(), msg)

		// Store counter to DB
		counter, counterErr := processor.getUserCounter()

		if counterErr != nil {
			return
		}

		counter.All--
		_, updateCounterErr := processor.updateUserCounter(counter)

		if updateCounterErr != nil {
			return
		}

		// Publish new event
		processor.streamingComp.PublishMessage(UserCounterIncreasedEvent, natsutil.Message{
			ID:      message.ID,
			Payload: message.Payload,
		})
	}, stan.DurableName(DurableID))
}

// Handle ActionRollbackUserCreation
func (processor userCounterProcessor) subscribeActionRollbackUserCreation() {
	// Get the NATS Streaming Connection
	sc := processor.streamingComp.NATS()
	sc.QueueSubscribe(account.ActionRollbackUserCreation, QueueGroup, func(msg *stan.Msg) {
		message := natsutil.Message{}
		err := json.Unmarshal(msg.Data, &message)

		if err != nil || message.Payload == nil || message.ID == uuid.Nil {
			log.Printf("%s received bad message: %+v\n", processor.streamingComp.Name(), msg)
			return
		}

		// Handle the message
		log.Printf("%s rolling back transaction with ID :%s", processor.streamingComp.Name(), message.ID)
	}, stan.DurableName(DurableID))
}
